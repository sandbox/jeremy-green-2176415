CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation


INTRODUCTION
------------

Current Maintainer: Jeremy Green <green@mutatedcreativity.com>

This module is a Drupal port of the KSS living styleguide methodology using the
Composer library. The module supports CSS and Sass based commenting. It will
scan your default theme and generate a style guide based on comments in your
Sass, Less or CSS files following a certain syntax. Visit
http://warpspire.com/kss/ for more information on KSS.


INSTALLATION
------------

* Install as usual, see
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

* You will need to have composer -- https://getcomposer.org -- installed and
working. Once the module is downloaded, navigate to the module directory and
run `composer install`. This will download all the related files and generate
the composer.lock file.

* Once composer has downloaded all the files, enable the module and enjoy.
