(function ($) {
  Drupal.behaviors.living_styleguide = {
    attach: function(context, settings) {

      var KssStateGenerator;

      KssStateGenerator = (function() {

        function KssStateGenerator() {
          var rule, stylesheet, _i, _j, _len, _len1, _ref, _ref1;
          this.theme_path = Drupal.settings.living_styleguide.theme_path;

          try {
            _ref = this.getStyleSheets();
            for (_i = 0, _len = _ref.length; _i < _len; _i++) {
              stylesheet = _ref[_i];
              if (stylesheet.href) {
                //not inline
                if (this.isThemeCSS(stylesheet.href)) {
                  this.processSelector(stylesheet);
                }
              } else {
                //must be inline
                _ref1 = stylesheet.cssRules;
                _len1 = _ref1.length;

                //loop through inline styles
                for (_f = 0; _f < _len1; _f++) {
                  //find import rules
                  if (_ref1[_f] && _ref1[_f].type === CSSRule.IMPORT_RULE) {
                    rules = stylesheet.cssRules;
                    for (_j = 0; _j < rules.length; _j++) {
                      rule = rules[_j].styleSheet;
                      if (this.isThemeCSS(rule.href)) {
                        this.processSelector(rule);
                      }
                    }
                  }
                }
              }
            }
          } catch (_error) { }
        }//init

        KssStateGenerator.prototype.getStyleSheets = function() {
          return document.styleSheets;
        };//getStyleSheets

        KssStateGenerator.prototype.handleRuleInsertion = function(rule) {
          var replaceRule, pseudos = /(\:hover|\:disabled|\:active|\:visited|\:focus|\:target)/g;

          if ((rule.type === CSSRule.STYLE_RULE) && pseudos.test(rule.selectorText)) {
              replaceRule = function(matched, stuff) {
                return matched.replace(/\:/g, '.pseudo-class-');
              };
              this.insertRule(rule.cssText.replace(pseudos, replaceRule));
            }
            pseudos.lastIndex = 0;
        }//handleRuleInsertion

        KssStateGenerator.prototype.insertRule = function(rule) {
          var headEl, styleEl;
          headEl = document.getElementsByTagName('head')[0];
          styleEl = document.createElement('style');
          styleEl.type = 'text/css';
          if (styleEl.styleSheet) {
            styleEl.styleSheet.cssText = rule;
          } else {
            styleEl.appendChild(document.createTextNode(rule));
          }
          return headEl.appendChild(styleEl);
        };//insertRule

        KssStateGenerator.prototype.isThemeCSS = function(href) {
          return (href.indexOf(this.theme_path) >= 0);
        }//isThemeCSS

        KssStateGenerator.prototype.processSelector = function(stylesheet) {
          var idx, _ref1, _len1;

          _ref1 = stylesheet.cssRules;
          for (idx = _j = 0, _len1 = _ref1.length; _j < _len1; idx = ++_j) {
            rule = _ref1[idx];
            this.handleRuleInsertion(rule);
          }
        }//processSelector;

        return KssStateGenerator;

        })();

      new KssStateGenerator
    }
  };
})(jQuery, Drupal);
