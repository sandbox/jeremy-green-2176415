<?php
/**
 * @file
 * Displays the modifiers.
 *
 * Available variables:
 * - $modifer: A modifer object.
 *
 * @see template_preprocess_living_styleguide_get_section()
 */
$extender = '';
$extender_label = '';
if ($modifier->isExtender()) :
  $extender = 'styleguide__element--extender';
  $extender_label = 'styleguide__element__modifier-label--extender';
endif;
?>
<div class="styleguide__element styleguide__element--modifier <?php print $extender; ?>"><!-- .styleguide__element -->
  <span class="styleguide__element__modifier-label <?php print $extender_label; ?>"><?php print $modifier->getName(); ?></span>
  <?php print $modifier->getExampleHtml(); ?>
</div><!-- /.styleguide__element -->
