<?php
/**
 * @file
 * Displays a living styleguide section.
 *
 * Available variables:
 * - $section: The section object.
 */
?>
<div class="styleguide" id="r<?php print $section->getReference(); ?>"><!-- .styleguide -->
  <h3 class="styleguide__header">
    <a href="#r<?php print $section->getReference(); ?>">
    <span class="styleguide__reference"><?php print $section->getReference(); ?></span>
    <span class="styleguide__title"><?php print $section->getTitle(); ?></span>
    </a>
  </h3>
  <div class="styleguide__description"><!-- .styleguide__description -->
  <p><?php print nl2br($section->getDescription()); ?></p>
    <?php if ($modifiers) : ?>
      <ul class="styleguide__modifiers">
      <?php foreach ($modifiers as $modifier) : ?>
      <?php print $modifier; ?>
      <?php endforeach; ?>
      </ul>
    <?php endif; ?>
  </div><!-- /.styleguide__description -->
  <div class="styleguide__elements"><!-- .styleguide__elements -->
  <div class="styleguide__element"><?php print $section->getMarkupNormal(); ?></div>
  </div><!-- /.styleguide__elements -->
  <div class="styleguide__html"><!-- .styleguide__html -->
  <pre class="styleguide__code"><code><?php print htmlentities($section->getMarkupNormal('{class}')); ?></code></pre>
  </div><!-- /.styleguide__html -->
</div><!-- .styleguide -->
